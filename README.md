# Task 33 - Angular Routing [x]

## Description
**Implementing Dashboard component and Lazy Loading**
 Continuation on previous projects:
 [T31](https://gitlab.com/Trocadelnero/task-31-services-and-http)
 [T32](https://gitlab.com/Trocadelnero/t32-forms-and-validations)

### App Includes, so far:
1. **LoginForm.component**:
- Reactive Forms and Validators.
2. **RegisterForm.component**:
- Reactive Forms.
- Validators: min/max-length, pattern="[a-zA-Z]*" and confirm-input for password.
3. **auth.Service**:
- register(user): Promise<any>
- login(user): Promise<any>
- HTTP request using the HttpClient to attempt register/login.

```
Personal TODO-
Implement from AngularPlayground and Course Angular series-videos:
- [X] AuthGuard 
canActivate

- [X] SessionService 
-Save and get token from http-request 

- [X] Environments
-Set base url to make use of easier routing. 

- [x] ReactiveForms and validators 
- Cleanup the forms *ngIfs
- Find solution for the Validators.maxlength [^1]

- [ ] Interfaces/models 
-Try to implement and make use of interfaces somewhere
- [] Users
- [] Surveys

- [X] Task TODO/Description- 
Convert Dashboard component to a Lazy Loaded Component 

- [X]Add 3 more components to application:
-SurveyList 
-SurveyListItem 
-SurveyDetail 

- [X] Add two new routes: (Both Lazy Loaded) 
-/surveys - Display the SurveyList component 
-/surveys/:id - Display the SurveyDetail component 

- [X] Add 1 new service 
-SurveyService 
-Service should have following methods: 

--getSurveys() - HTTP Request - Get a list of surveys (See API Document) 
--getSurvey(id) - HTTP Request - Get a single survey from a given survey id 


[^1] Lazy-fix: *ngIf="username.invalid && username.dirty">NOTICE: Passwords can not be more than 12 characters
        Problem Description: Validators.maxlength()
        had to be set as (11) if it was ="12" on the input
        to be able to make use of .errors.maxlength
        But if you still want to make use of the max amount allowed
        .errors.maxlength still persists which makes input.invalid.
        Instead used Lazy-fix.
```
### Additional features added thanks to Angular video-series:
1. **HttpInterceptor**:
- Identifies and handles a given HTTP request.
2. **catchError()**:
- Added some error-handling to easier error-message-formatting.


**This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.1.**

## Install dependencies.

Run `npm install`to install dependencies if you cloned this project.

## Development server

Run `ng serve -o` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

### Aknowledgements

- Api provided by our Great Teacher Dewald Els
[Api](https://documenter.getpostman.com/view/1167753/Szf6Y8xS?version=latest)


