import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }

  /**
   * Save Session
   * @param session 
   */
  save(session: any) { localStorage.setItem('sp_session', JSON.stringify(session)); }

  /**
   * Get Session
   */
  get(): any {

    const savedSession = localStorage.getItem('sp_session');

    return savedSession ? JSON.parse(savedSession) : false; }

  /**
   * Remove session
   */
  remove(): any { localStorage.removeItem('sp_session'); }

}
