import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {


  constructor(private session: SessionService, private router: Router) { }

  ngOnInit(): void {
  }

  get username() {
    return this.session.get().username;
  }


async onLogoutClicked() {
  try{
  this.session.remove()
} finally{
  this.router.navigateByUrl('/login');
}
}
}
