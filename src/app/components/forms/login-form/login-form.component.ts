import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from "src/app/services/auth/auth.service";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(3)]),
    password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(12)]),
  });

  isLoading: boolean = false;
  loginMessage: string;
  loginError: string;

  constructor(private auth: AuthService, private session: SessionService, private router: Router) {

    if (this.session.get() !== false) {
      this.router.navigateByUrl('/dashboard');
    }
  }
  ngOnInit(): void {

  }

  //Getters
  get username() {
    return this.loginForm.get('username');
  }
  get password() {
    return this.loginForm.get('password');
  }

  /**
   * Try async login
   * @param loginForm
   */
  async onLoginClicked() {
    this.loginError = '';
    this.loginMessage = '';

    try {
      this.isLoading = true;
      const { token, username } = await this.auth.login(this.loginForm.value);
      this.session.save({ token: token, username: username });
      this.router.navigateByUrl('/dashboard');

    } catch (e) {
    this.loginError = e;
    } finally {
      this.isLoading = false;
    }

  }

}

